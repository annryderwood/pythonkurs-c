# Aufgaben am 12.01.2023

## 1 – Wörterfinden


### Holt sie raus!
Zugegeben, die Aufgabe ist etwas albern, aber Indizierung und Slicing ist ein wichtiges Konzept und das hier ist 
seeeeeeehr anschaulich...

Findet die Wörter und holt sie mit Indizierung heraus:
zB: Trottel => rot => "Trottel"[1:4]

- Reblaus
- Kugelblitz
- Schund
- Dachschindel
- Scherz
- Baugerüst
- Baderaum

<details> 
  <summary>Tipps </summary>
   Schaut Euch den Code an, den wir in der Stunde besprochen haben und wandelt ihn stückweise ab.
</details>

### Findet sie und holt sie raus...
In den folgenden Wörtern sind Wörter versteckt, aber Ihr braucht dann auch die Schrittweite, also wort[5:11:3] oder so!
```
überzüchten 
angepfiffen
sinneszelle
nachbohren 
drehbeginn 
losrattern 
backsteinern
asienreise
```

<details> 
  <summary>Die gesuchten Wörter (Spoiler)</summary>
```
überzüchten -> brühe
angepfiffen -> neffe
sinneszelle -> insel
nachbohren -> ahorn
drehbeginn -> rhein
losrattern -> orten
backsteinern -> aktien
asienreise -> serie
```
</details>


## 2 – Listen korrigieren

Hier sind Listen, die irgendwie falsch sind. Findet den Fehler (oder denkt Euch selbst einen aus :D) und korrigiert 
ihn so:
```python
rgb_farben = ["rot", "grün", "schwarz"]
rgb_farben[2] = "blau"

print(rgb_farben)
```

```python
wochentage = ["Montag", "Dienstag", "Mittwoch", "Schokoladentag", "Freitag", "Samstag", "Sonntag"]
planeten = ["Merkur", "Aphrodite", "Erde", "Mars", "Jupiter", "Saturn", "Neptun", "Pluto"]
phasen_des_debugging = ["So, jetzt noch schnell eine Klammer zu und dann ist es fertig! Ausführen!",
                        "Oh. Warum funktioniert das denn nicht?",
                        "Wie konnte das jemals funktionieren?",
                        "Ich habe das Gefühl, dass ich in einem Labyrinth aus Code gefangen bin.",
                        "Ich frage mich, ob ich jemals wieder das Licht des Tages sehen werde.",
                        "Ich werde meine Seele verkaufen, nur um diesen Fehler zu finden.",
                        "Ich habe das Gefühl, dass ich in einem endlosen Loop feststecke.",
                        "Ich bin sicher, dass dieser Code mein Untergang sein wird."]
```

Hier müsst Ihr es sogar mit Slicing, nicht nur mit Indexing machen:

```python
jahreszeiten = ["Frühling", "Sommer", "Herbst", "Winter", "Wartezeit"]
monate = ["Januar", "Februar", "April", "Mai", "Juni", "Juli", "August", "September", "Oktober", "November", "Dezember"]
zahlwoerter = ["eins", "zwei", "drei", "sechs", "sieben", "acht"]
```

<details> 
  <summary>Tipps </summary>
   Wenn Ihr stecken bleibt, schlagt im Internet nach! Formuliert auf Deutsch, was Ihr machen wollt, übersetzt es 
auf Englisch und gebt es in eine Suchmaschine ein. Schaut Euch dort die Beispiele an.
</details>

## 3 – Farben und random
Schreibt ein Programm, das fünf zufällig gefärbte Quadrate nebeneinander malt!

So etwas zum Beispiel:

![](../Abbildungen/Zufallspalette.png)