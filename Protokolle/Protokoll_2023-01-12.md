# Pikos Python Kurs am 12.01.2023
Im Laufe der Woche findet sich noch die Besprechung der dritten Hausaufgabe auf: https://diode.zone/c/pykurs_c/videos


## Pikos Dateien
Matheübungsprogramm: wichtiges Detail
```python
from turtle import *
import random

a = random.randint(1, 100)
b = random.randint(1, 100)
result = a + b 
print(a,b, result)
antwort = input("Was ist das Ergebnis aus " + str(a) + " + " + str(b) + " ?")

print("Das Ergebnis der Zufallszahlen ist " + str(a) + "+" + str(b) + "=" + str(result) +"." )

print("result", type(result))
print("antwort", type(antwort))

# if result == str(antwort): #"43" != 43
# if str(result) == antwort:
if result == int(antwort):
    print("Richtig!")
else:
    print("Falsch!")
```
Hausaufgaben
```python
from turtle import *

schrittweite = 10
schritte = 20
zeilen = 20
stiftdicke = 10
pensize(stiftdicke)

# 
# color(1, 0, 0)
# fd(20)
# color(1, 0, 1)
# fd(20)

for j in range(zeilen):
    for i in range(schritte):
        pencolor(0, j/zeilen, i/schritte)
        fd(schrittweite)
    penup()
    fd(-1 * schrittweite * schritte)
    lt(90)
    fd(stiftdicke)
    rt(90)
    pd()
```
```python
from turtle import *

anzahl_kreise = 20
stiftdicke = 10
pensize(stiftdicke + 1)

# 
# color(1, 0, 0)
# fd(20)
# color(1, 0, 1)
# fd(20)

for i in range(anzahl_kreise):
    pencolor(0, 0, 1-i/anzahl_kreise)
    pu()
    fd(stiftdicke * (i+1))
    lt(90)
    pd()
    circle(stiftdicke * (i+1))
    pu()
    rt(90)
    fd(-1 * stiftdicke * (i+1))
    
# 0, 0.05, 0.1, 0.15, 0.2, 0.25, 0.3, 0.35 .... 0.95
# 1, 0.95, 0.9, 0.85, 0.8, 0.75, 0.7, 0.65
```
Indizierung und Slicing
```python
# INDIZIERUNG
# INDEX
# INDIZES

wort = "Fluffizitätsskandal"

print(wort[3])
# print(wort[30])
print(wort[0])
print(wort[-1])

print(wort[3+8])
print(len(wort))
print(wort[18])


# SLICING

print(wort[3:10])
```
Slicing
```python
wort = "zerknitterterer"
#         x  x  x  x  x

print(wort[3:])
print(wort[:4] + wort[4:])

print(wort[2:10:3])
print(wort[2::3])
print(wort[12:3:-1])

print(type(wort[3]))
print(type(wort[3:8]))


obstliste = ["apfel", "banane", "heidelbeeren", "erdbeeren", "litschis", "ananas", "papaya", "mango", "apfel", "birne"]
print(obstliste[1:4])

print(type(obstliste[3]))
print(type(obstliste[3:8]))
```